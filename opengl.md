# OPENGL

## Intro / Libraries

### GLEW (OpenGL Extension Wrangler)
Allows to interface with modern OpenGL and handle platform-specific extensions.

### GLFW
gives the possibility to create windows and OpenGL contexts and handle user input.

### SDL
SDL does all that GLFW does and more.

---
## Shaders and rendering Pipelines

### Rendering pipeline

- Series of stages that take place in order to render an image to the screen
- Four stages are programmable via Shaders
	- Vertex Shader
	- Fragment Shader
	- Geometry Shader
	- Tessellation Shader
	- Fifth one, Compute shader, very new
- Shaders are pieces of code written in GLSL (OpenGL Shading Language) or HLSL (High-Level Shading Language) in Direct3D
- GLSL is based on C

##### Rendering pipeline stages

1. Vertex specification
2. Vertex Shader (programmable)
3. Tessellation (programmable)
4. Geometry Shader (programmable)
5. Vertex Post-Processing
6. Primitve Assembly
7. Rasterization
8. Fragment Shader (programmable)
9. Per-Sample operations

### [Meet the Shaders : Vertices, Polygons and Meshes](https://medium.com/@darkdreamday/meet-the-shaders-vertices-polygons-and-meshes-1dde115c4bc6)
### [Inside out : A Shader’s Anatomy](https://medium.com/@darkdreamday/inside-out-a-shaders-anatomy-51d006291a45)

##### 1. Vertex Specification

- A vertex (plural: vertices) is a point in space, usually defined with x, y and z co-ordinates.
- A primitive is a simple shape defined using one or more vertices.
- Uses VAOs (Vertex Array Objects) and VBOs (Vertex Buffer Objects).
- VAO defines WHAT data a vertex has (position, colour, texture, normals, etc).
- VBO defines the data itself.

##### 2. Vertex Shader

- Handles vertices individually.
- Can specify additional outputs that can be picked up and used by user-defined shaders later in pipeline.
- Inputs consist of the vertex data itself.

##### 3. Tesselation

- Allows you to divide up data in to smaller primitives.
- Can be used to add higher levels of detail dynamically.

##### 4. Geometry Shader

- Vertex Shader handles vertices, Geometry Shader handles primitives (groups of vertices).
- Takes primitives then “emits” their vertices to create the given primitive, or even new primitives.
- Can alter data given to it to modify given primitives, or even create new ones.
- Can even alter the primitive type (points, lines, triangles, etc).

##### 5. Vertex Post-Processing

###### Transform Feedback

Result of Vertex and Geometry stages saved to buffers for later use.

###### Clipping

Clipping doesn't take in to account which side of an object is facing the camera, it simply determines if things are within the projection's frustum. It literally checks which vertices are outside the frustum and removes them from the system before running the calculations to render to the screen, that way it can avoid trying to do unnecessary drawing of things that we can't even see! the vertices that were removed are replaced by ones new ones that are placed right on the edge of the frustum.

##### 6. Primitive Assembly

Vertices are converted in to a series of primitives.So if rendering triangles 6 vertices would become 2 triangles (3 vertices each).

###### Face culling

Is the removal of primitives that can’t be seen, or are facing “away” from the viewer. We don’t want to draw something if we can’t see it!

##### 7. Rasterization
![reg](./img/rasterization.png)
- Converts primitives in to “Fragments”.
- Fragments are pieces of data for each pixel, obtained from the rasterization process.
- Fragment data will be interpolated based on its position relative to each vertex.

##### 8. Fragment Shader

- Handles data for each fragment.
- Most important output is the colour of the pixel that the fragment covers.

##### 9. Per-Sample Operations

- Series of tests run to see if the fragment should be drawn.
- Most important test: Depth test. Determines if something is in front of the point being drawn.
- Colour Blending: Using defined operations, fragment colours are “blended” together with overlapping fragments. Usually used to handle transparent objects.
- Fragment data written to currently bound Framebuffer.
- In the application code the user usually defines a buffer swap here, putting the newly updated Framebuffer to the front.

##### [glVertexAttribPointer clarification](https://stackoverflow.com/questions/8704801/glvertexattribpointer-clarification)

---
## Vector, Matrices, Uniform variables

- Vectors are directions and positions in space.
- Matrices are 2-dimensional arrays of data used for calculating transforms and various other functions.
- Vectors are a type of matrix and can have these functions applied to them.
- The order of transform operations matters!!
- Last matrix operation applied happens first.
- Uniform variables pass global data to shaders.
- Need to obtain a uniform’s location then bind data to it.

### Matrix Transforms - Combining

ORDER MATTERS!

- First multiply transforms, then apply to vector

![reg](./img/combine.png)

![reg](./img/combine2.png)

---

## Interpolation, Indexed Draws and Projections

### Interpolation

- Happens during the Rasterization stage.
- In other words: A weighted average of the three verices on a triangle is passed on
- Fragment Shader picks up the interpolated value and uses that.
- The value is effectively an estimate of what the value should be at that position, had we defined it ourself.

### Indexed Draws

Refer to multiple defined vertices by index.
- Just bind them to and Element Array Buffer in the VAO.
- Unbind the IBO AFTER you unbind the VAO.
- A VAO can only have one IBO, if you unbind it before the VAO it tells the VAO to unbind the IBO. Therefore, we first unbind the VAO and then unbind the IBO because the VAO is no longer being altered.

### Projections

- Used to convert from “View Space” to “Clip Space”.
- Can be used to give the scene a ‘3D’ look.
- Alternatively can be used to create a 2D style for projects that require it.

##### Projections: Co-ordinate Systems

- Local Space: Raw position of each vertex drawn relative to origin. Multiply by Model Matrix to get...
- World Space: Position of vertex in the world itself if camera is assumed to be positioned at the origin. Multiply by View Matrix to get...
- View Space: Position of vertex in the world, relative to the camera position and orientation. Multiply by Projection Matrix to get...
- Clip Space: Position of vertex in the world, relative to the camera position and orientation, as viewed in the area not to be “clipped” from the final output.
- Screen Space: After clipping takes place, the final image is created and placed on the co-ordinate system of the window itself.

To create Clip Space we define an area (frustum) of what is not to be clipped with a Projection Matrix.
Two commonly used types of Projection:
- Orthographic (most common in 2D applications)
- Perspective (most common in 3D applications)

##### Projections: Orthographic

![reg](./img/ortho.png)

- Cuboid frustum
- Everything between the Far Plane and Near Plane is kept. Rest is discarded.
- Parallel nature of Orthographic means 3D depth is not visible.
- Move object closer/further and it won’t change size on screen.

##### Projections: Perspective

![reg](./img/perspective.png)

- The frustum is a “truncated pyramid”.
- Each pixel on Near Plane diverges at an angle to reach matching point on Far Plane.
- Gives the illusion of depth.

##### Projections: Comparsion

![reg](./img/comparsion.png)

- Orthographic: The one furthest back looks to be the same size as the one at the front, implying it’s actually larger.
- Perspective: The one at the back look smaller than the one at the front, due to it being more distant, as it should.

##### Projections: Summary

- Interpolation calculates weighted values between vertices during rasterization.
- Indexed Draws let us define vertices once then reference them to draw them.
- Projection Matrices convert View Space in to Clip Space.
- Orthographic Projections are used for 2D applications and don’t allow depth perception.
- Perspective Projections are for 3D applications and create the illusion of depth.

---
## Camera and User input

### Camera / View Space

![reg](./img/camera_view.png)

- The Camera processes the scene as seen in “View Space”.
- View Space is the co-ordinate system with each vertex as seen from the camera.
- Use a View Matrix to convert from World Space to View Space.
- View Matrix requires 4 values: Camera Position, Direction,Right and Up.
- Camera Position: Simply the position of the camera.
- Direction: The direction the camera is looking in.
- Direction vector actually points in opposite direction of the intuitive “direction”.
- Right: Vector facing right of the camera, defines x-axis. Can calculate by doing cross product of Direction and “up” vector [0, 1, 0].
- Up: Upwards relative to where camera is facing. Can calculate by doing cross product of Direction and Right vectors.

![reg](./img/view_matrix.png)
- Place values in matrices to calculate View Matrix.
- View Matrix applied to a vertex will convert it to View Space.

### Using the view Matrix

- Bind the view matrix to a uniform on the shader
- Apply it between the **projection** and **model** matrices
- ORDER MATTERS, multiplying in different order will not work

### Input: moving the camera

- GLFW: glfGetKey(window, GLFW_KEY_W)
- SDL: Check for event, check keydown event, check which key
- Add the value to the camera position
- Different CPU speeds will have different movements!

### Input: Delta Time

- Basic idea: Check time passed since last loop, apply maths to keep consistent speed

deltaTime = currentTime - lastTime;
lastTime = currentTime;
Multiply the cameras movement speed by deltaTime.

[Timestep Fix](https://gafferongames.com/post/fix_your_timestep/)

### Input: Turning

There are 3 types of angles:
- Pitch: Up and down
- Yaw: left and right
- Roll: doing a roll (like a plane)
- Pitch uses an axis relative to the yaw
- Yaw rotates around the y-axis

##### Input: Turning - Pitch

![reg](./img/turn_pitch.png)
- Pitch will depend on yaw, update x,y,z
- x = cos(pitch)
- y = sin(pitch)
- z = cos(pitch)

##### Input: Turning - Yaw

![reg](./img/turn_yaw.png)
- x = cos(yaw)
- z = sin(yaw)

##### Input: Turning - Pitch and Yaw

- x = cos(pitch) x cos(yaw)
- y = sin(pitch)
- z = cos(pitch) x sin(yaw)

### Input: Summary

- View Matrix requires Position, Direction, Right and Up vectors.
- Delta times allows consistent speed on different systems
- Turning uses Pitch, Yaw and Roll in some cases
- Compare current and last posistion to determine pitch and yaw change.

---
## Textures and Image loading

### Textures

![reg](./img/texel.png)
- Used for extra detail on an object
- Can be used to hold generic data
- Usually 2D, but also 1D/3D possible
- Points are texels not pixels
- Texels are defined between 0 and 1
- Map texels to vertices
- Interpolation over each fragment calculates appropriate texels between assigned texels

### Texture Objects

- Works much like VBO/VAO
- glGenTextures(1, &texture); glBindTexture(GL_TEXTURE_2D, texture);
- glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data);
	- First Argument: Texture Target (what we have bound our texture to)
	- Second Argument: Mipmap Level (see next slide)
	- Third Argument: Format of the stored data. RGB is just Red, Green Blue values. Also RGBA which has an Alpha Channel (transparency). Several others, not important here.
	- Fourth and Fifth Argument: Width and height of the texture.
	- Sixth Argument: This should always be 0. Legacy concept handling texture borders that OpenGL doesn’t utilise anymore.
	- Seventh Argument: Format of the data being loaded (as opposed to stored on the third argument).
	- Eighth Argument: The data type of the values (int, float, byte, etc).
	- Ninth Argument: The data itself.

### Mimamps

![reg](./img/mipmap.png)
- Resolution limitations for textures
- The closer, the more pixelated the texture. Further away it attempts to render multiple texels on one pixel.
- Solution: Multiple versions of image at different resolutions to switch between based on distance

### Texture Params - Filters

![reg](./img/filter.png)
- Rendering of off center texels
	- Nearest: Use texel with most overlap (pixelated effect, ex: retro game)
	- Linear: Use weighted average of surronding texels (blends pixel boundaries) More common
- glTexParameter: used to set texture rendering parameters.

### Texture Params - Wrap

![reg](./img/wrap_param.png)

- Sample a point outside of 0, 1 range
- Ways to handle it:
	- Repeat texture
	- Repeat a mirrored form of the texture
	- Extend pixels at the edge
	- Apply colored border
- Use glTexParameter

### Loading images for textures

- Write own image loader
- Use a library like SOIL or stb_image

### Texture Samplers

- Textures in shaders are accessed via samplers
- textures are attached to a texture unit
- Samplers accesses textures attached to their texture unit
- Example:
	***Texture*** ---is attached to---> ***Texture Unit 0*** \<---reads from texture attached to--- ***sampler2D: 0***

### Texture Units

![reg](./img/texture_unit.png)

- Bind texture to desired Texture Unit:
		- glActiveTexture(GL_TEXTURE0);
		- glBindTexture(GL_TEXTURE_2D, textureID);
- Ensure sampler2D variables know which Texture Unit to access:
		- glUniform1i(uniformTextureSampler, 0);
- Value attached to uniform is the Texture Unit number.

### Summary

- Textures use texels between 0, 1
- Texels are bound to vertices, values are interpolated
- Mipmaps handle levels of detail more efficiently
- Texture Filtering changes how texels are blended based on size on the screen
- Texture Wrapping changes how texture are handled for texels outside of 0,1
- Wrapping and Filtering --> glTexParameteri
- Images loading with 3rd party lib for convenience
- Textures attach to texture units, samplers read from textures attached to the texture units

---

## Phong Lighting and Directional lights

### Phong Lighting
![reg](./img/phong.png)
- 3 Parts together create the Phong Lighting Model
	- Ambient Lighting: Is always present, even if light source's path is blocked
	- Diffuse Lighting: Determined by direction of light, creates a faded effect further from light
	- Specular Lighting: Reflected perfectly from source to the eye. Reflection of light source. Prominent on shiny objects

##### Ambient lighting

- Simplest lighting object
- Simulates light bouncing off other objects
- Global illumination simulates this

- Create an ambient lighting factor.
	- ambient = lightColour * ambientStrength;
- This factor is how much of a fragment’s colour this light’s ambient shows.
	- fragColour = objectColour * ambient;
- If ambient is 1 (full power) then the fragment is always fully lit.
- If ambient is 0 (no power) then the fragment is always black.
- I ambient is 0.5 (half power) then fragment is half its normal colour.

##### Diffuse Lighting
- More complex
- Simulates the drop-off of lighting as angle of lighting becomes more shallow
- Side facing directly at a light is brightly lit.
- Side facing at an angle is more dim.
- Can use the angle between the vector connecting light source to fragment and the vector perpendicular to the face (surface “normal”).

- Use θ to determine Diffuse Factor.
	- Smaller θ: More light.
	- Larger θ: More dim.

![diffuse lighting](./img/diffuse_lighting.png)
- Recall from Vectors: Dot Product.
	- v1 · v2 = |v1| x |v2| x cos(θ)
- If both vectors are normalized (converted to unit vectors...
	- Then: |v1| = |v2| = 1
- So: v1 · v2 = cos(θ)
- Since cos(0 degrees) = 1, and cos(90 degrees) = 0
- We can use the output of v 1 ·v 2 to determine a diffuse factor!

- Normalize normal and light vectors
	![reg](./img/diffuse_90.png)
	- v1 ·v2 = cos(θ) = cos(90 degrees) = 0
	- Diffuse factor of 0 (0% diffuse lighting)!
	![reg](./img/diffuse_0.png)
	- v1 ·v2 = cos(θ) = cos(0 degrees) = 1
	- Diffuse factor of 1 (100% diffuse lighting)!
	![reg](./img/diffuse_45.png)
	- v1 ·v2 = cos(θ) = cos(45 degrees) = 0.71
	- Diffuse factor of 0.71 (71% diffuse lighting)!
	- If factor is negative (less than 0) then light is behind surface, so default to 0.

- Apply diffuse factor with ambient:
	- fragColour = objectColour * (ambient + diffuse);

###### Diffuse Lighting - Normals

- Are perpendicular to their point on a surface
- Each vertex will have multiple normals, one for each face it is part of.
- Good for “Flat Shading”, not good for realistic smooth shading.
- Also doesn’t work too well for indexed draws: We only define each vertex once per face.

- ALTERNATIVE: Phong shading (!Phong Lighting)
- Each vertex has an average of the normals of all the surfaces it is part of.
- Interpolate between these averages in shader to create smooth effect.
- Good for complex models! Not so good for simple models with sharp edges (unless you use some clever modelling techniques).
- Phong Shaded sphere is defined the same way as Flat Shaded.
![reg](./img/phong_shading.png)
- Smoothness is illusion created by interpolating and effectively “faking” surface normals to look curved.

![reg](./img/skewing.png)

- Problem with non-uniform scales or Wrongly skewing normals can be countered by creating a “normal matrix” from model matrix.
	- Transform normals with: mat3(transpose(inverse(model)))

[FULL EXPLANATION](http://www.lighthouse3d.com/tutorials/glsl-12-tutorial/the-normal-matrix/)

### Specular lighting

- Specular relies on the position of the viewer.
- It is the direct reflection of the light source hitting the viewer’s eye.
- Moving around will affect the apparent position of the specular reflection on the surface.
- Therefore, we need four things:
	- Light vector
	- Normal vector
	- Reflection vector (Light vector reflected around Normal)
	- View vector (vector from viewer to fragment)

![reg](./img/specular.png)
Need the angle between viewer and reflection.
- Smaller θ: More light.
- Larger θ: More dim.

- View vector is just the difference between the Fragment Position and the Viewer (Camera) position.
- Reflection vector can be obtained with a built-in GLSL function: reflect(incident, normal)
	- Incident: Vector to reflect.
	- Normal: Normal vector to reflect around.
- Just as with diffuse, use dot product between normalized forms of view and refection vector, to get specular factor.

##### Specular lighting - shininess

![reg](./img/shininess.png)
- One last step to alter Specular Factor: Shininess.
- Shininess creates a more accurate reflection.
- Higher shine: Smaller more compact specular.
- Lower shine: Larger, faded specular.
- Simply put previously calculated specular factor to the power of shininess value.
- specularFactor = (view · reflection)^shininess

- Then apply Specular Factor as with ambient and diffuse...
- fragColour = objectColour * (ambient + diffuse + specular);

### Types of Light

- Directional Light: A light without a position or source. All light is coming as parallel rays from an seemingly infinite distance. Best example is the Sun.
- Point Light: A light with a position that shines in all directions. Best example is a lightbulb.
- Spot Light: Similar to a Point Light, but cut down to emit in a certain range, with a certain angle. Best example is a flashlight.
- Area Light: More advanced light type that emits light from an area. Think of a large light up panel on a wall or ceiling.

### Summary

- Phong Lighting Model combines Ambient, Diffuse and Specular lighting.
- Diffuse and Specular require normals.
- Use dot product and normals to determine diffuse lighting.
- Use dot product and light reflected around normals to determine specular lighting.
- Phong Shading interpolates averaged normals to create smooth surfaces.
- Four main types of light: Directional, Point, Spot and Area.
-  Directional Light is easiest, since it only requires a direction and allows us to skip calculating a light vector!

---
## Point Lights and Spot Lights

### Point Lights

![reg](./img/point_light.png)
- Lights with a position that emit light in ALL directions.
- Need to determine direction vector manually.
- Get difference between light position and fragment position.
- Apply directional lighting maths to the calculated direction vector.

### Point Lights – Attenuation

- Directional Lights simulate infinite distance, so distance doesn’t effect lighting power.
- Point Lights have positions, distance from point being lit changes power of lighting.
- One possible solution: Linear drop-off. Have the lighting power drop off in direct proportion with the distance of the light source. Simple, but not very realistic...

![reg](./img/point_attenuation.png)
- In reality, light intensity initially drops quickly with distance. But the further you are, the slower it decreases!
- For positive values, the reciprocal of a quadratic function can create this effect!
- In other words: 1/(ax2 + bx + c)  Where x is the distance between the light source and fragment.

![reg](./img/point_attenuation_form.png)
- distance: Distance between light and fragment.
- quadratic: User-defined value, usually the lowest of the three.
- linear: User-defined value, lower than constant.
- constant: Usually 1.0 to ensure denominator is always greater than 1.
- E.g. if denominator is 0.8, then 1.0/0.5 = 2.0, so attenuation will DOUBLE power of light beyond its set value.
- For useful values see: http://wiki.ogre3d.org/tiki-index.php?page=-Point+Light+Attenuation
- Alternatively, toy around with values!
- Application: Apply attenuation to ambient, diffuse and specular.

### Spot Lights

- Work the same as Point Lights in theory. Have position, use attenuation, etc. Also have a direction and a cut-off angle.
- Direction: Where the spot light is facing.
- Cut-off angle: The angle describing the “edges” of the light, from the direction vector.
- Need a way to compare the “Angle to Fragment” to the “Cutoff Angle”. Use the Dot Product!
![reg](./img/spot_light.png)
- angleToFragment = lightVector · lightDirection
- lightVector: The vector from the light to the fragment.
- lightDirection: The direction the Spot Light is facing.
- So angleToFragment will be a value between 0 and 1, representing the angle between the two.
- Can simply do cos(cutOffAngle) for the Cut Off Angle.
- Larger value: Smaller angle.
- Smaller value: Larger angle.
- If angleToFragment value is larger than cos(cutOffAngle),then it is within the spot: Apply lighting. If angleToFragment value is smaller, then it has a greater angle than the Cut Off: Don’t apply lighting.

### Spot Lights - Soft Edges

- Current approach has sharp cut-off at edges of spot. Creates unrealistic spot light (although might be good for retro video game effect).
- Need a way to soften when approaching edges of cut-off range. Use the dot product result from before as a factor!
- Issue: Due to select range, dot product won’t scale very well.
- E.g. If cut-off is 10 degrees...
- Minimum dot product is cos(10 degrees) = 0.98
- Dot product range will be 0.98 – 1.00
- Using dot product to fade edge will be almost unnoticeable!
- Solution: Scale dot product range to 0 – 1!

- Formula to scale value between ranges:
![reg](./img/spot_light_form.png)
- newRangeMin is 0, newRangeMax is 1, so numerator is just originalValue – originalRangeMin.
- originalRangeMax is 1.
- So with some inverting of min and max values.
- (cos value of angleToFragment etc.)
![reg](./img/spot_light_form2.png)

- After calculating Spot Light lighting Multiply by spotLightFade effect.
- colour = spotLightColour * spotLightFade

### Summary

- Point Lights emit light in all directions.
- Use Directional Light algorithm with light vector.
- Fade light with distance via attenuation values.
- Spot Lights are Point Lights with a direction and cut-off range.
- Compare light vector angle with cut off angle.
- Soften edges with scaled form of light vector angle.
